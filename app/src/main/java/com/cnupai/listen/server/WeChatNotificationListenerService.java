package com.cnupai.listen.server;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.service.notification.NotificationListenerService;
import android.service.notification.StatusBarNotification;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;


import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class WeChatNotificationListenerService extends NotificationListenerService {
    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        //Log.i("xiaolong", "open" + "-----" + sbn.getPackageName());
        Log.i("xiaolong", "open" + "------" + sbn.getNotification().tickerText);
        Log.i("xiaolong", "open" + "-----" + sbn.getNotification().extras.get("android.title"));
        Log.i("xiaolong", "open" + "-----" + sbn.getNotification().extras.get("android.text"));
        Map data = new HashMap();
        data.put("packageName", sbn.getPackageName());
        data.put("tickerText", sbn.getNotification().tickerText);
        data.put("title", sbn.getNotification().extras.get("android.title"));
        data.put("text", sbn.getNotification().extras.get("android.text"));
        JSONObject json = new JSONObject(data);
        Intent intent = new Intent();
        intent.putExtra("notificationInfo", json.toString());
        intent.setAction("location.reportsucc");
        sendBroadcast(intent);
    }

    @Override
    public void onNotificationRemoved(StatusBarNotification sbn) {
        Log.i("xiaolong", "remove" + "-----" + sbn.getPackageName());
    }

    //重新开启NotificationMonitor
    public void toggleNotificationListenerService() {
        ComponentName thisComponent = new ComponentName(this,  WeChatNotificationListenerService.class);
        PackageManager pm = getPackageManager();
        pm.setComponentEnabledSetting(thisComponent, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
        pm.setComponentEnabledSetting(thisComponent, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);

    }

    private static boolean isNotificationListenerServiceEnabled(Context context) {
        Set<String> packageNames = NotificationManagerCompat.getEnabledListenerPackages(context);
        if (packageNames.contains(context.getPackageName())) {
            return true;
        }
        return false;
    }
}
