package com.cnupai.listen.server;

import java.util.List;
import android.app.ActivityManager;
import android.content.Context;

import org.json.JSONArray;

import io.dcloud.common.DHInterface.IWebview;
import io.dcloud.common.DHInterface.StandardFeature;
import io.dcloud.common.util.JSUtil;

public class Util extends StandardFeature {
    public static boolean isServiceRunning( ActivityManager activityManager, String className) {
        boolean isRunning = false;
        /*ActivityManager activityManager = (ActivityManager) mContext
                .getSystemService(Context.ACTIVITY_SERVICE);*/

        List<ActivityManager.RunningServiceInfo> serviceList = activityManager.getRunningServices(100);
        if (serviceList.size() == 0) {
            return false;
        }
        for (int i = 0; i < serviceList.size(); i++) {
            if (serviceList.get(i).service.getClassName().equals(className) == true) {
                isRunning = true;
                break;
            }
        }
        return isRunning;
    }

    public void PluginTestFunction(IWebview pWebview, JSONArray array)
    {
        String CallBackID = array.optString(0);
        JSONArray newArray = new JSONArray();
        JSUtil.execCallback(pWebview, CallBackID, "12121", JSUtil.OK, false);

    }

}
